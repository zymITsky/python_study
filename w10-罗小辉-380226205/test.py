def extendList(val, list=[]):
    list.append(val)
    return list


list1 = extendList(10)
list2 = extendList(123, [])
list3 = extendList('a')

print("list1 = %s" % list1)
print("list2 = %s" % list2)
print("list3 = %s" % list3)


# def a():
#     print('a executed')
#     return []
#
#
# def b(x=a()):
#     print('id(x):', id(x))
#     x.append(5)
#
#
# for i in range(2):
#     print('*' * 20)
#     b()
#     print('b.__defaults__:', b.__defaults__, ';b.__annotations__', b.__annotations__, ";b.__call__()", b.__call__(),
#           ";b.__closure__", b.__closure__, ";b.__code__", b.__code__, ";b.__dict__", b.__dict__,
#           ";b.__doc__", b.__doc__, ";b.__globals__", b.__globals__,
#           ";b.__hash__()", b.__hash__(), ";b.__init__()", b.__init__(), ";b.__module__", b.__module__,
#           ";b.__name__", b.__name__, ";b.__sizeof__()", b.__sizeof__(), ";b.__str__()", b.__str__())
    # print('id(b.__defaults__[0]):', id(b.__defaults__[0]))

# for i in range(2):
#     # 写20个*，python有这种写法，也是够够的了
#     print('*' * 20)
#     b(list())
# print('b.__defaults__:', b.__defaults__)
# print('id(b.__defaults__[0]):', id(b.__defaults__[0]))
