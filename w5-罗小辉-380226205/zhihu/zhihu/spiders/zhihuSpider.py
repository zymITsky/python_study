# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request, FormRequest
import ssl
import os
import urllib.request
from scrapy.http import Request
from lxml import etree as etree

ssl._create_default_https_context = ssl._create_unverified_context

host = "https://www.zhihu.com/"


class ZhihuspiderSpider(scrapy.Spider):
    name = 'zhihuSpider'
    allowed_domains = ['zhihu.com']

    # start_urls = ['https://www.zhihu.com/']

    headers = {
        'Host': 'www.zhihu.com',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
        'Cache-Control': 'max-age=0',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.zhihu.com',
        'Connection': 'keep-alive',
        'Cookie': '_zap=c6418a3b-17c7-4904-a94c-2d23bb04b153; _ga=GA1.2.553414766.1455708210; _za=676364ba-05fb-4796-b030-4a76cd51e2e7; d_c0="AEAAm2TEsgmPTtmimFocrE4AoaXONRxJ6Rk=|1459395683"; _zap=e42a2918-7875-4161-9977-42411f5f75ed; q_c1=adcc810c2bd6425a92f8ca46ecfb8a13|1508844276000|1459395683000; q_c1=adcc810c2bd6425a92f8ca46ecfb8a13|1512299244000|1459395683000; l_cap_id="NjAxNzhkNDliMGI5NGIzY2FlZDc3MWIyMGY2ZGRmYWQ=|1512560105|03ab9bf8f56acad639445bbde96da13f25ff937c"; r_cap_id="NjdmNDQ0MzA5NWY5NDA1ZDhjMWE0OTNlZmEwMGM1NDY=|1512560105|15c3cae05133eebdd3f19832e4b7d418a1f4d3bb"; cap_id="YTUwZWYyM2FjNmEwNGFkOGIzYTk4ODI4Y2EyNDBkNmE=|1512560105|160f81b967d7a9d09f4429830d21de81ca79df85"; __utma=51854390.553414766.1455708210.1512552788.1512557506.10; __utmz=51854390.1512557506.10.3.utmcsr=zhihu.com|utmccn=(referral)|utmcmd=referral|utmcct=/topics; __utmv=51854390.000--|2=registration_date=20151216=1^3=entry_date=20160331=1; aliyungf_tc=AQAAAJFkPjwrrgMA1RyptNcN5TtYMql1; _xsrf=cc8e938f-c5f0-4b15-a0e9-9867e89ce316',
        'Upgrade-Insecure-Requests': '1'
    }

    def start_requests(self):
        return [
            Request("https://www.zhihu.com/topics", meta={"cookiejar": 1}, headers=self.headers, callback=self.parse)]

    def parse(self, response):

        topicNames = response.xpath("//li[@class='zm-topic-cat-item']/a/text()").extract()
        topicIds = response.xpath("//li[@class='zm-topic-cat-item']/@data-id").extract()

        # 获取某话题下所有主题的信息
        themeLink = response.xpath("//div[@class='item']/div[@class='blk']/a[@target='_blank']/@href").extract()
        themeName = response.xpath(
            "//div[@class='item']/div[@class='blk']//a[@target='_blank']/strong/text()").extract()
        themeDesc = response.xpath("//div[@class='item']/div[@class='blk']/p/text()").extract()

        self.get_theme_by_topicId(topicIds=topicIds, topicNames=topicNames)

    def get_theme_by_topicId(self, topicIds, topicNames):
        for topicIndex in range(len(topicIds)):
            print("================================正在抓取话题中" + topicNames[topicIndex] + "下各个主题的数据================================")
            link = host + themeLink[themeIndex]
            self.get_theme(link, themeName[themeIndex])
        pass

    def get_theme(self, url, themeName):

        print("------------------------正在抓取主题中" + themeName + "下各个问题的数据------------------------")
        print(themeName + "链接为-->" + url)
        themeData = urllib.request.urlopen(url).read().decode("utf-8", "ignore")
        treeData = etree.HTML(themeData)

        questionLink = treeData.xpath("//a[@class='question_link']/@href")
        questionName = treeData.xpath("//a[@class='question_link']/text()")
        questionAuthor = treeData.xpath("//a[@class='authon-link']/text")

        for questionIndex in range(len(questionName)):
            link = host + questionLink[questionIndex]
            print(questionName[questionIndex])

    def next(self, response):
        print(response.body.decode("utf-8", "ignore"))
        title = response.xpath("/html/head/title/text()").extract()
        print("准备打印标题。。。")
        print(title)
        print("标题打印完全。。。")

    # 获取验证码
    def get_captcha(self, url):
        localpath = "captcha/"
        fileName = localpath + "captcha.png"
        if os.path.exists(localpath) == False:
            os.makedirs(localpath)

        urllib.request.urlretrieve(url, fileName)
