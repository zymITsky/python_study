# coding=utf-8

import urllib
from urllib import request
import ssl
import random
import re
from bs4 import BeautifulSoup
import requests
import xlwt
import os

ssl._create_default_https_context = ssl._create_unverified_context
uapools = [
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)",
    "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.5; AOLBuild 4337.35; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)",
    "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322)",
    "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.3 (Change: 287 c9dfb30)",
    "Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.6",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2pre) Gecko/20070215 K-Ninja/2.1.1",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9) Gecko/20080705 Firefox/3.0 Kapiko/3.0",
    "Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Fedora/1.9.0.8-1.fc10 Kazehakase/0.5.6",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20",
    "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
]


def ua(uapools):
    thisua = random.choice(uapools)
    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
               'Accept-Encoding': 'gzip, deflate',
               'Accept-Language': 'zh-CN,zh;q=0.8',
               'Cache-Control': 'no-cache',
               'Connection': 'keep-alive',
               'Host': 'search.jd.com/',
               'Pragma': 'no-cache',
               'Referer': 'https://search.jd.com/Search?keyword=%E7%BE%BD%E7%BB%92%E6%9C%8D%E7%94%B7&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=%E7%BE%BD%E7%BB%92%E6%9C%8D%E7%94%B7&page=1&click=0',
               'User-Agent': thisua}
    opener = urllib.request.build_opener()
    opener.addheaders = headers
    urllib.request.install_opener(opener)


def get_html(url):
    ua(uapools)
    data = requests.get(url, timeout=10).content.decode("utf-8", "ignore")
    # data = urllib.request.urlopen(url).read().decode("utf-8", "ignore")
    return data


# 定义一个类装载基本数据
class BaseItem:
    def __init__(self, productName="", brands="", price="", sellers="", commentsCount="", linkAddr=""):
        self.productName = productName
        self.brands = brands
        self.price = price
        self.sellers = sellers
        self.commentsCount = commentsCount
        self.linkAddr = linkAddr

    def getProductName(self):
        return  self.productName

baseItem = BaseItem()
baseItem.getProductName()

baseItemList = []


def getEveryPageData(url):
    data = BeautifulSoup(get_html(url), 'lxml')
    prices = data.findAll('div', 'p-price')
    comments = data.findAll('div', 'p-commit')
    goodsAttrs = data.findAll('div', 'p-name p-name-type-2')
    stores = data.findAll('div', 'p-shop')

    priceList = []
    for index in range(len(prices)):
        priceDom = prices[index - 1]
        if priceDom.strong.attrs.get('data-done', '0') == '1':
            price = priceDom.strong.i.contents[0]
            priceList.append(price)

    for index in range(len(priceList)):
        goodsAttr = goodsAttrs[index]
        price = priceList[index]
        comment = comments[index].strong.a.contents[0]
        if stores[index].span != None:
            sellers = str(stores[index].span.a.contents[0])
        else:
            sellers = "没有店名"

        brands = str(goodsAttr.em.contents[0])

        if len(goodsAttr.em.contents) == 2:
            productName = str(goodsAttr.em.contents[1])
        elif len(goodsAttr.em.contents) == 1:
            productName = str(goodsAttr.em.contents[0])
        else:
            productName = str(goodsAttr.em.contents[2])
        linkAttr = str(goodsAttr.a.attrs['href'])

        print("商品厂牌-->" + brands)
        print("商品名称-->" + productName)
        print("专卖店名字-->" + sellers)
        print("商品价格-->" + price)
        print("链接地址-->" + linkAttr)
        print("评论数量-->" + comment)

        baseItem = BaseItem(productName, brands, price, sellers, comment, linkAttr)
        baseItemList.append(baseItem)


def saveDataToExcel():
    biaotou = ['商品厂牌', '商品名称', '专卖店名字', '商品价格', '链接地址', '评论数量']
    # 当前文件夹下搜索的文件名后缀
    fileform = "xls"
    # 将合并后的表格存放到的位置
    filedestination = "excel/"

    filename = xlwt.Workbook()
    sheet = filename.add_sheet(keyword)
    # 下面是把表头写上
    for i in range(0, len(biaotou)):
        sheet.write(0, i, biaotou[i])

    if os.path.exists(filedestination) == False:
        os.makedirs(filedestination)

    print("总共有" + str(len(baseItemList)) + "条数据,正准备插入数据到excel中。。。。")
    for index in range(1, len(baseItemList)):
        # for keyPostion in range(0,6):
        baseItem = baseItemList[index]
        sheet.write(index, 0, baseItem.brands)
        sheet.write(index, 1, baseItem.productName)
        sheet.write(index, 2, baseItem.sellers)
        sheet.write(index, 3, baseItem.price)
        sheet.write(index, 4, baseItem.linkAddr)
        sheet.write(index, 5, baseItem.commentsCount)

    filename.save(filedestination + keyword + "." + fileform)
    print("数据保存成功。。。")


def getJDDataByKey(keyword, pages):
    key = urllib.request.quote(keyword)
    for page in range(1, pages):

        if page % 2 == 1:
            print("正在爬取第" + str(int((
                                        page + 1) / 2)) + "页的数据-----------------------------------------------------------------------------------------------------------------------------------------------------------")
            url = "https://search.jd.com/Search?keyword=" + key + "&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=" + key + "&page=" + str(
                page) + "&click=0"
        else:
            url = "https://search.jd.com/Search?keyword=" + key + "&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=" + key + "&page=" + str(
                page) + "&click=0&scrolling=y"
        getEveryPageData(url)
    saveDataToExcel()


keyword = "零食"
getJDDataByKey(keyword, 100)
# saveDataToExcel()
