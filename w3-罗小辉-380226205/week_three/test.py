# coding=utf-8

import urllib.request
import re

def get_html(url):
    return urllib.request.urlopen(url).read().decode("utf-8", "ignore")

# class="pic_list_show" name="fsfm"
url = "https://www.zhihu.com/topics"
data = get_html(url)
print(data)
