# coding=utf-8

import numpy as npy
import pandas as pda
from sklearn.linear_model import LogisticRegression as LR
from sklearn.linear_model import RandomizedLogisticRegression as RLR

file_name = "data/luqu.csv"
dataf = pda.read_csv(file_name)
x = dataf.iloc[:, 1:4].as_matrix()
y = dataf.iloc[:, 0:1].as_matrix()
r1 = RLR()
r1.fit(x, y)
# 特征筛选
print(r1.get_support())
print(dataf.columns[r1.get_support()])
# t = dataf[dataf.columns[r1.get_support()]].as_matrix()
# # 进入逻辑回归
# r2 = LR()
# r2.fit(x, y)
# r2.score(x, y)
# rst = r2.predict(x)
# x2 = npy.array([[660, 3.61, 2]])
# rst2 = r2.predict(x2)
# print(rst2)
