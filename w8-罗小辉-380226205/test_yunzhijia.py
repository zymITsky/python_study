#coding=utf-8

import urllib
import requests

test_data = {'ServiceCode':'aaaa','b':'bbbbb'}
test_data_urlencode = urllib.urlencode(test_data)

requrl = "https://analytics.yunzhijia.com/i?checksum=b39f118883d200854d7b9ff02a300b47f3b2c87e"
# json = "{'app_key':'12d9e8c372a3ea5161152e3736d060c0af8597cc'," \
#        "'timestamp':'1515491622774'," \
# "'hour':'18'," \
# "'dow':'2'," \
# "'events':'[{"key":"[CLY]_view","count":1,"timestamp":1515491612804,"hour":17,"dow":2,"segmentation":{"name":"com.kdweibo.android.ui.activity.StartActivity","visit":"1","start":"1","segment":"Android"},"sum":0},{"key":"upgrade_type","count":1,"timestamp":1515491615369,"hour":17,"dow":2,"segmentation":{"label":"自有升级"},"sum":0},{"key":"[CLY]_view","count":1,"timestamp":1515491615452,"hour":17,"dow":2,"segmentation":{"name":"com.kdweibo.android.ui.activity.StartActivity","segment":"Android","dur":"3"},"sum":0},{"key":"[CLY]_view","count":1,"timestamp":1515491615461,"hour":17,"dow":2,"segmentation":{"name":"com.kdweibo.android.ui.fragment.HomeMainFragmentActivity","visit":"1","segment":"Android"},"sum":0},{"key":"bottombar_msg","count":1,"timestamp":1515491615674,"hour":17,"dow":2,"sum":0},{"key":"websocket","count":1,"timestamp":1515491615819,"hour":17,"dow":2,"segmentation":{"连接状态":"websocket_status_open","连上重试次":"0"},"sum":0},{"key":"FirstPage","count":1,"timestamp":1515491615934,"hour":17,"dow":2,"sum":0},{"key":"upgrade_type","count":1,"timestamp":1515491616040,"hour":17,"dow":2,"segmentation":{"label":"自有升级"},"sum":0},{"key":"[CLY]_view","count":1,"timestamp":1515491616673,"hour":17,"dow":2,"segmentation":{"name":"com.kdweibo.android.ui.fragment.HomeMainFragmentActivity","segment":"Android","dur":"1"},"sum":0},{"key":"[CLY]_view","count":1,"timestamp":1515491622755,"hour":17,"dow":2,"segmentation":{"name":"com.yunzhijia.checkin.activity.MobileCheckInActivity","visit":"1","segment":"Android"},"sum":0}]'," \
# "'timestamp':'1515491622774'," \
# "'timestamp':'1515491622774'," \
#        "}"
headers = {
    "Content-Type":"application/x-www-form-urlencoded",
    "User-Agent":"Dalvik/2.1.0 (Linux; U; Android 7.1.1; OD103 Build/NMF26F)",
    "Host":"analytics.yunzhijia.com",
    "Connection":"Keep-Alive",
    "Accept-Encoding":"gzip"
}

data = requests.post(requrl, timeout=10,headers=headers).content.decode("utf-8", "ignore")

print(data)