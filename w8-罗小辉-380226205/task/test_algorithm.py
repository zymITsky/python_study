# coding=utf-8
import datetime
import pandas as pda
from algorithms.FP_grow_tree import FP_Grow_tree
from algorithms.apriori import Apriori



def loadDataSet(cls, file_name):
    # 数据加载
    print("===========================================================================")
    print("               初始化：正在加载数据：" + file_name)
    print("===========================================================================")
    all_matrix = pda.read_excel(file_name, header=1)
    product_types = all_matrix.columns[1:]
    for type_name in product_types:
        cols = all_matrix[type_name]
        cols[cols == "T"] = type_name
        cols[cols == "F"] = None
    dataSet = []
    for indexs in all_matrix.index:
        iarray = all_matrix.loc[indexs].values[1:-1]
        dataSet.append(iarray[iarray != None])
    return dataSet

def catulate_with_apriori(cls, dataSet, min_support=0.5):
    start_time = datetime.datetime.now().microsecond
    L, supportData = Apriori.caculate(dataSet, min_support)
    sorted_supportData = sorted(supportData.items(), key=lambda item: item[1], reverse=True)
    time_diff = datetime.datetime.now().microsecond - start_time
    for item in sorted_supportData:
        if len(item[0]) >= 2 and item[1] >= min_support:
            print(
                "               频繁" + str(len(item[0])) + "项集：" + str(item[0]) + ", 支持度：" + format(item[1], '.2%'))
    print("===========================================================================")
    print("               算法[apriori]耗时：" + str(time_diff) + "毫秒")
    print("===========================================================================")

def catulate_with_fp_growth(cls, dataSet, min_support=200):
    start_time = datetime.datetime.now().microsecond
    ff = FP_Grow_tree.FP_Grow_tree(dataSet, [], min_support)
    time_diff = datetime.datetime.now().microsecond - start_time
    ff.printfrequent()
    print("===========================================================================")
    print("               算法[fp_growth]耗时：" + str(time_diff) + "毫秒")
    print("===========================================================================")




data_file = "../超市数据集.xls"

min_support = 0.35
dataSet = loadDataSet(data_file)
catulate_with_apriori(dataSet, min_support)

min_support = 200
catulate_with_fp_growth(dataSet, min_support)

