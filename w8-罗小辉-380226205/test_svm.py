import numpy as npy
from sklearn import svm
import matplotlib.pyplot as plt

'''加载数据'''
x1 = []
y1 = []
for i in range(0, 10):
    if (i <= 3 or i >= 8):
        x1.append([i, i])
        y1.append(0)
    else:
        x1.append([i, i])
        y1.append(1)

x = npy.array(x1)
y = npy.array(y1)
'''创建SVM'''
# 线性核函数
linear = svm.SVC(kernel='linear').fit(x, y)
# 多项式核函数
poly = svm.SVC(kernel='poly', degree=4).fit(x, y)
# 径向基核函数
rbf = svm.SVC().fit(x, y)
#
sigmoid = svm.SVC(kernel='sigmoid').fit(x, y)
a = 1
x21, x22 = npy.meshgrid(npy.arange(x[:, 0].min(), x[:, 0].max(), 0.01), npy.arange(x[:, 1].min(), x[:, 0].max(), 0.01))
for i in [linear, poly, rbf, sigmoid]:
    rst = i.predict(npy.c_[x21.ravel(), x22.ravel()])
    # plt.subplot(横向划分，纵向划分, 定位)
    plt.subplot(2, 2, a)
    # contourf 等高线
    plt.contourf(x21, x22, rst.reshape(x21.shape))
    # yk 设置不同颜色
    # 训练数据的点也绘制出来
    for j in range(0, len(y1)):
        if (int(y1[j]) == 0):
            plt.plot(x[j:j + 1, 0], x[j:j + 1, 1], 'yo')
        else:
            plt.plot(x[j:j + 1, 0], x[j:j + 1, 1], 'ko')
    a += 1
plt.show()