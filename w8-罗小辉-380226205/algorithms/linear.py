from numpy import *
#import matplotlib.pyplot as plt
#from pylab import *
#单特征
def load1(filename):
    fh=open(filename,"r")
    tznum=1
    dataMat=[]#[[0.13],[0.14],[0.12]]
    labelMat=[]
    #dataMat2主要格式为列表，放特征值
    dataMat2=[]#[0.13,0.14,0.12]
    for line in fh.readlines():
        lineArr=[]
        valueArr=line.strip().split("\t")
        for value in range(0,tznum):
            lineArr.append(float(valueArr[value]))
        dataMat.append(lineArr)
        labelMat.append(float(valueArr[-1]))
        dataMat2.extend(lineArr)
    fh.close()
    return dataMat,labelMat,dataMat2
'''
a,b,c=load1('a.txt')
print(a)
print(b)
print(c)
'''
def solvedo(x,y):
    o={}
    x=mat(x)
    y=mat(y).T
    x0=mat(ones((x.shape[0],1)))
    x=hstack((x0, x))
    xTx=x.T*x
    #print(xTx)
    o=xTx.I*(x.T*y)
    return o
#test1适用于预测数据与训练数据一致
def test1(x,y):
    x=mat(x)
    o=solvedo(x,y)
    x0=mat(ones((x.shape[0],1)))
    x=hstack((x0, x))
    return (mat(x)*o).T.getA()[0]
#test2适用于预测数据与训练数据不一致
def test2(x,o):
    x=mat(x)
    x0=mat(ones((x.shape[0],1)))
    x=hstack((x0, x))
    #print((mat(x)*o).T)
    return (mat(x)*o).T.getA()[0]
a,b,c=load1('a.txt')
rst1=test1(a,c)
#多特征（多元线性回归）
def load2(filename):
    fh=open(filename,"r")
    tznum=len(fh.readline().split("\t"))-1#最后一个不算特征
    dataMat=[]
    labelMat=[]
    for line in fh.readlines():
        lineArr=[]
        valueArr=line.strip().split("\t")
        for value in range(0,tznum):
            lineArr.append(float(valueArr[value]))
        dataMat.append(lineArr)
        labelMat.append(float(valueArr[-1]))
    fh.close()
    return dataMat,labelMat
'''
a,b=load2('b.txt')
print(a[1:10])
print(b[1:10])
'''
a,b=load2('b.txt')
rst2=test1(a,b)
