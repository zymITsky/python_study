# coding=utf-8

import requests
import re
from bs4 import BeautifulSoup
import os

def get_html(url):
    return requests.get(url).content.decode("utf-8")


# 抓取首页文章链接和文章title
def crawl_csdn():
    url = "http://blog.csdn.net"
    html = get_html(url)
    pat_link = '<h3.{1,}href="(.*?)"\s{1,}target='
    pat_title = 'h3.{1,}target="_blank">\s{0,}(.*?)</a>'
    link = re.compile(pat_link).findall(html)
    title = re.compile(pat_title).findall(html)
    for i in range(len(link)):
        save_data(link[i], title[i])
        print("第" + str(i + 1) + "个首页文章链接--->" + link[i])
        print("第" + str(i + 1) + "个首页文章title--->" + title[i])


# 抓取每篇文章地址并保存到本地
def save_data(url, title):
    html = get_html(url)
    save_dir = "csdn/"
    save_address = save_dir + title + ".html"
    print(save_address)
    if os.path.exists(save_dir) == False:
        os.makedirs(save_dir)
    fh = open(save_address, 'w')
    fh.write(html)
    fh.close()

crawl_csdn()
