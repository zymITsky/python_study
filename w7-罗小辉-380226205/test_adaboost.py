# coding=utf-8

from numpy import *
from math import log
from math import e


def dcstump_class(data, i, k, thresh):
    a, b = shape(data)
    classrst = ones((a, 1))  # ones，生成a行1列的一维数组
    if k == 0:
        # 左子树
        classrst[data[:, i] <= thresh] = -1
    elif k == 1:
        # 右子树
        classrst[data[:, i] > thresh] = -1
    return classrst


# 单层决策树
def dcstump(data, labels, D):
    a, b = shape(data)
    allstep = 10
    minerror = inf  # 正无穷
    for i in range(0, b):
        max1 = data[:, i].max()
        min1 = data[:, i].min()
        step = (max1 - min1) / allstep
        for j in range(-1, int(allstep) + 1):
            # 计算当前阀值
            thresh = min1 + j * step
            # 接下来进入类别循环
            for k in range(0, 2):
                classrst = dcstump_class(data, i, k, thresh)
                errorarr = ones((a, 1))
                errorarr[classrst[:, 0] == labels] = 0
                errorarr = mat(errorarr)
                weighterror = D.T * errorarr
                if (weighterror < minerror):
                    # 统计出最小错误率的那一次阀值等信息
                    minerror = weighterror
                    besti = i
                    bestthresh = thresh
                    bestk = k
                    bestclass = classrst.copy()
    return minerror, besti, bestthresh, bestk, bestclass


def adaboost_train(data, labels, iternum=40):
    a, b = shape(data)
    D = ones((a, 1)) / a
    weak = []
    weakclass = zeros((a, 1))
    for i in range(0, iternum):
        minerror, besti, bestthresh, bestk, bestclass = dcstump(data, labels, D)
        alpha = 1 / 2 * log((1 - minerror) / minerror, e)
        ex = multiply(-1 * alpha * mat(labels).T, bestclass)
        D = multiply(D, exp(ex))
        D = D / D.sum()
        best = {"besti": besti, "bestthresh": bestthresh, "bestk": bestk, "alpha": alpha}
        weak.append(best)
        weakclass += alpha * bestclass
        weakerrors = multiply(sign(weakclass) != mat(labels).T, ones((a, 1))).sum()
        # print(wearerros)
        if weakerrors == 0:
            break;
    return weak


def adaboosttest(data, weak):
    a, b = shape(data)
    weakclass = zeros((a, 1))
    for i in range(0, len(weak)):
        classrst = dcstump_class(data, weak[i]["besti"], weak[i]["bestk"], weak[i]["bestthresh"])
        weakclass += weak[i]["alpha"] * classrst
    return sign(weakclass)


data = array([[1.3, 1],
              [1, 1],
              [2, 1],
              [2, 1.1],
              [1, 2.1]])
labels = [1.0, 1.0, -1.0, -1.0, -1.0]
a = adaboost_train(data, labels)
data2 = array([[1, 1]])
result = adaboosttest(data2, a)
print(result)
