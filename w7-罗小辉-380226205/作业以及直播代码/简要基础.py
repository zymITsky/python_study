import pandas as pda
import numpy as npy
filename="D:/Python35/data/luqu.csv"
dataf=pda.read_csv(filename)
x=dataf.iloc[:,1:4].as_matrix()
y=dataf.iloc[:,0:1].as_matrix()
#2、数据归一化
from sklearn import preprocessing
#归一化处理，处理0-1之间
nx=preprocessing.normalize(x)

#标准化处理，减去平均值，然后除以方差，结果是聚集在0附近，方差是1
sx=preprocessing.scale(x)


#切分
from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(sx,y,test_size=0.2, random_state=20)

