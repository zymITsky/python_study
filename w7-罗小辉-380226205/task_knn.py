# coding=utf-8

import csv
from numpy import *
import operator

# knn算法
# 从列方向扩展
# tile(a,(size,1))
def knn(k, testdata, traindata, labels):
    # testdata:一维数组[0,0,1,……]
    # traindata：二维数组[[0,0,1……],[],]
    # labels：一维列表，跟traindata一一对应
    # 以下shape取的是训练数据的第一维，即其行数，也就是训练数据的个数
    traindatasize = traindata.shape[0]
    dif = tile(testdata, (traindatasize, 1)) - traindata
    # tile()的意思是给一维的测试数据转为与训练数据一样的行和列的格式
    sqdif = dif ** 2
    # axis=1-----》横向相加的意思
    sumsqdif = sqdif.sum(axis=1)
    # sumsqdif在此时已经成为1维的了
    distance = sumsqdif ** 0.5
    sortdistance = distance.argsort()
    # sortdistance为测试数据到各个训练数据的距离按近到远排序之后的结果
    count = {}
    for i in range(0, k):
        vote = labels[sortdistance[i]]
        # sortdistance[i]测试数据最近的K个训练数据的下标
        # vote测试数据最近的K个训练数据的类别
        count[vote] = count.get(vote, 0) + 1
    sortcount = sorted(count.items(), key=operator.itemgetter(1), reverse=True)
    return sortcount[0][0]


# 获取训练数据
def get_train_data():
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)

    rows = 0
    for line in reader:
        rows += 1
    print("开始获取训练数据。。。")
    train_arr = zeros((rows, 4))
    flower_types = []
    index = 0
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)
    for line in reader:
        flower_types.append(line[4])
        train_arr[index, :] = data_to_array(line)
        index += 1

    print("--------总共有" + str(rows) + "个数据集训练完成--------")
    return train_arr, flower_types


# 获取测试数据
def get_test_data(fractions):
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)

    rows = 0
    for line in reader:
        rows += 1
    print("开始获取测试数据。。。")
    index = 0
    flower_types = []
    test_arr = zeros((int(rows / fractions), 4))
    index = 0
    count = 0
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)
    for line in reader:
        # 把训练数据中1/fractions的数据，作为测试数据
        if index % fractions == 0:
            flower_types.append(line[4])
            test_arr[count, :] = data_to_array(line)
            count += 1
        index += 1
    print("--------------总共有" + str(len(test_arr)) + "个测试数据采集完成--------------")
    return test_arr, flower_types


# 数据转数组
def data_to_array(line):
    array = []
    for i in range(0, len(line) - 1):
        array.append(line[i])
    return array


# 测试knn算法的准确度
def test_knn_accuracy(fractions):
    print("开始测试knn算法的准确度。。。")
    train_arr, train_types = get_train_data()
    test_arr, test_types = get_test_data(fractions)
    rightCount = 0
    for i in range(len(test_arr)):
        test_arr_ = test_arr[i]
        flower_type = knn(3, test_arr_, train_arr, train_types)
        if flower_type == test_types[i]:
            rightCount += 1
    print("knn算法的准确率为:" + str(rightCount / (len(test_types))))


# 参数表示把训练数据中1/fractions的数据，作为测试数据
test_knn_accuracy(3)

# #抽某一个测试文件出来进行试验

