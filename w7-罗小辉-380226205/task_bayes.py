# coding=utf-8

# 贝叶斯算法的应用
from numpy import *
import operator
from os import listdir
import numpy as npy
import numpy
import csv


class Bayes:
    def __init__(self):
        self.length = -1
        self.labelcount = dict()  # 各类别的概率["类别1":概率1, "类别2":概率2]
        self.vectorcount = dict()  # 以字典方式存储类别和特征向量
        # 格式为{"类别1":[特征向量1，特征向量2,...],...,"类别n":[特征向量1，特征向量2,...]}

    def fit(self, dataSet: list, labels: list):
        if (len(dataSet) != len(labels)):
            raise ValueError("您输入的训练数组跟类别数组长度不一致")

        self.length = len(dataSet[0])  # 测试数据特征值的长度
        labelsnum = len(labels)  # 类别所有的数量(可重复)
        norlabels = set(labels)  # 不重复类别的数量
        print(str(labelsnum) + "--" + str(norlabels))
        print(type(labels))
        for item in norlabels:
            thislabel = item
            # 求的当前类别总共出现了多少次
            self.labelcount[thislabel] = labels.count(thislabel) / labelsnum
            # 通过zip将两个数组交叉放置，比如：
        '''
        x1 = [a, b, c]
        x2 = [e, f ,g]
        k = zip(x1, x2)
        k:[(a,e),(b,f),(c,g)]
        '''
        for vector, label in zip(dataSet, labels):
            if (label not in self.vectorcount):
                self.vectorcount[label] = []
            self.vectorcount[label].append(vector)
        print("训练结束")
        return self

    def btest(self, TestData, labelsSet):
        if (self.length == -1):
            raise ValueError("您还没有进行训练，请先训练")

        # 计算testdata分别为各个类别的概率
        lbDict = dict()  # ["类别1":概率, "类别2":概率2]
        for thislb in labelsSet:
            p = 1
            alllabel = self.labelcount[thislb]  # 当前类别的概率p(c)
            allvector = self.vectorcount[thislb]  # 当前类别的所有特征向量
            vnum = len(allvector)
            allvector = numpy.array(allvector).T  # 转置一下
            '''
            如
            原来的是[[t1,t2,t3,t4],[t1,t2,t3,t4]]
            现在成为:[t1,t1],[t2,t2],[t3,t3],[t4,t4]

            '''
            for index in range(0, len(TestData)):  # 依次计算各特征的概率
                vector = list(allvector[index])
                p *= vector.count(TestData[index]) / vnum  # p(当前特征|C)
                # 如testdata[0]:0,vector:[1,0,1,0,0,1,1],p为3/7
            lbDict[thislb] = p * alllabel  # alllabel相当于p(c)
        thislabel = sorted(lbDict, key=lambda x: lbDict[x], reverse=True)[0]
        return thislabel


# 建立训练数据
# 获取训练数据
def get_train_data():
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)

    rows = 0
    for line in reader:
        rows += 1
    print("开始获取训练数据。。。")
    train_arr = zeros((rows, 4))
    flower_types = []
    index = 0
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)
    for line in reader:
        flower_types.append(line[4])
        train_arr[index, :] = data_to_array(line)
        index += 1

    print("--------总共有" + str(rows) + "个数据集训练完成--------")
    return train_arr, flower_types


# 获取测试数据
def get_test_data(fractions):
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)

    rows = 0
    for line in reader:
        rows += 1
    print("开始获取测试数据。。。")
    index = 0
    flower_types = []
    test_arr = zeros((int(rows / fractions), 4))
    index = 0
    count = 0
    csvfile = open('iris.csv')
    reader = csv.reader(csvfile)
    for line in reader:
        # 把训练数据中1/fractions的数据，作为测试数据
        if index % fractions == 0:
            flower_types.append(line[4])
            test_arr[count, :] = data_to_array(line)
            count += 1
        index += 1
    print("--------------总共有" + str(len(test_arr)) + "个测试数据采集完成--------------")
    return test_arr, flower_types


# 数据转数组
def data_to_array(line):
    array = []
    for i in range(0, len(line) - 1):
        array.append(line[i])
    return array


# 测试贝叶斯算法的准确度
def test_knn_accuracy(fractions):
    print("开始测试贝叶斯算法的准确度。。。")
    bys = Bayes()
    train_data, labels = get_train_data()
    bys.fit(train_data, labels)

    test_arr, test_types = get_test_data(fractions)
    labelsall = ["setosa", "versicolor", "virginica"]
    rightCount = 0
    for i in range(len(test_arr)):
        test_arr_ = test_arr[i]
        thislabel = test_types[i]
        label = bys.btest(test_arr_, labelsall)
        print("该鸢尾花是：" + str(thislabel) + ",识别出来的鸢尾花是:" + str(label))
        if label == thislabel:
            rightCount += 1
    print("贝叶斯算法的准确率为:" + str(rightCount / (len(test_types))))


test_knn_accuracy(2)
