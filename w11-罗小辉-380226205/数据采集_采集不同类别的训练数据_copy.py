# coding=utf8

import urllib.request
import re
import pymysql

conn = pymysql.connect(host="127.0.0.1", user="root", password="123456", db="machine_learning", charset="utf8")


def get_data(pdnum, class_type, detail):
    # 获取当前频道所有作品
    all_novels = []
    for i in range(0, 3):
        try:
            this_page = urllib.request.urlopen(
                "http://www.itangyuan.com/category/" + pdnum + "_" + str(i) + ".html").read().decode("utf-8", "ignore")
            pat1 = '<span class="bname".*?<a href="/book/(.*?).html">'
            print("*" * 20 + "正准备获取" + "http://www.itangyuan.com/category/" + pdnum + "_" + str(
                i) + ".html" + "中的数据" + "*" * 20)
            novel_id = re.compile(pat1, re.S).findall(this_page)
            for j in novel_id:
                print("小说id-->" + str(novel_id))
                all_novels.append(j)
        except Exception as err:
            print(err)
    # 获取每部作品中的文章
    all_articles = []
    for i in all_novels:
        try:
            this_url = "http://www.itangyuan.com/book/" + str(i) + ".html"
            this_page = urllib.request.urlopen(this_url).read().decode("utf-8", "ignore")
            pat1 = 'id="chapter_.*?_url" href="/book/chapter/(.*?).html"'
            article_id = re.compile(pat1, re.S).findall(this_page)
            for j in article_id:
                print(j)
                all_articles.append(j)
        except Exception as err:
            print(err)

    # 获取每篇文章的内容
    for i in all_articles:
        try:
            this_url = "http://www.itangyuan.com/book/chapter/" + i + ".html"
            this_page = urllib.request.urlopen(this_url).read().decode("utf-8", "ignore")
            pat1 = '<div class="section-main-con" data-name="section-con">(.*?) <p class="end">'
            content = re.compile(pat1, re.S).findall(this_page)
            pat2 = '<a class="book-name" href="/book/.*?.html">(.*?)</a>'
            title = re.compile(pat2, re.S).findall(this_page)
            if (len(content) == 0):
                continue
            else:
                content = content[0]
            if (len(title) == 0):
                continue
            else:
                title = title[0]
            sql = "insert into novel(content, title, classtype, detail) VALUES (\'%s\',\'%s\',\'%s\',\'%s\')" % (
            str(content), str(title), str(class_type), str(detail));
            conn.query(sql)
            conn.commit()
            print("插入数据库成功。。。")
        except Exception as err:
            print("插入数据库失败。。。")
            print(err)


pdnum = ["girl_4408_0_heat_0", "girl_193_0_heat_0", "boy_182_0_heat_0", "girl_22_0_heat_0"]
detail = ["现言", "古风", "悬疑", "灵异"]
class_type = ["0", "1", "2", "3"]
all_web = zip(pdnum, detail, class_type)
for i in all_web:
    get_data(i[0], i[1], i[2])
