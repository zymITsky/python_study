# coding=utf-8

import os
import jieba
import pymysql
import re
import numpy as npy
import pandas as pda
from sklearn.feature_extraction.text import CountVectorizer

conn = pymysql.connect(host="127.0.0.1", user="root", passwd="123456", db="machine_learning", charset="utf8")
sql = "select title,content from article limit 1000"


def loaddata(data):
    data1 = jieba.cut(data)
    data11 = ""
    for item in data1:
        data11 += item + " "
    return data11


dataf = pda.read_sql(sql, conn)
dataf2 = dataf.T
title = dataf2.values[0]
content = dataf2.values[1]
train_text = []
for i in content:
    thisdata = loaddata(i)
    # [^abc]匹配除a,b,c之外的任意字符
    # pat1 = "<[^>]*?>"，其实就是去掉标签<>,不管标签<>中有什么内容，都可以整体去掉
    pat1 = "<[^>]*?>"
    # re是regular expression的所写，表示正则表达式
    # sub是substitute的所写，表示替换；
    # re.sub()表示将thisdata中所有的字符串pat1替换成"",区别于replace，sub可以用正则
    thisdata = re.sub(pat1, "", thisdata)
    print(thisdata)
    print("*" * 30)
    thisdata = thisdata.replace("\n", "").replace("\t", "")
    train_text.append(thisdata)

# sklearn中的特征抽取类CountVectorizer
'''
词库模型将文档转换成词块的频率构成的特征向量，用CountVectorizer类计算基本单词频次的二进制特征向量
'''
count_vect = CountVectorizer()
train_x_counts = count_vect.fit_transform(train_text)
# tfidf模型
'''
TF-IDF（Term Frequency–Inverse Document Frequency）是一种用于资讯检索与文本挖掘的常用加权技术。
TF-IDF是一种统计方法，用以评估一个字词对于一个文件集或一个语料库中的其中一份文件的重要程度。
字词的重要性随着它在文件中出现的次数成正比增加。
'''
from sklearn.feature_extraction.text import TfidfTransformer

tf_ts = TfidfTransformer(use_idf=True).fit(train_x_counts)
train_x_tf = tf_ts.transform(train_x_counts)
from sklearn.cluster import KMeans

# 聚类 KMeans
'''
聚类简单的说就是要把一个文档集合根据文档的相似性把文档分成若干类，但是究竟分成多少类，这个要取决于文档集合里文档自身的性质。
另外聚类是典型的无指导学习，所谓无指导学习是指不需要有人干预，无须人为文档进行标注。
'''
kms = KMeans(n_clusters=3)
y = kms.fit_predict(train_x_tf)
dataf["type"] = y
dataf.to_csv("./01聚类归类法结果.csv", encoding='gbk')

'''
在此网页分类聚类方案中，
首先将文档集用CountVectorizer类计算字词的二进制特征向量，
然后用TF-IDF计算各字词在文档集中出现的频率，
最后用聚类KMeans进行无指导学习分类
'''