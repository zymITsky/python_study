import os
import jieba
import re
import pymysql
import pandas as pda
from sklearn.feature_extraction.text import CountVectorizer

conn = pymysql.connect(host="127.0.0.1", user="root", password="123456", db="machine_learning", charset="utf8")
sql = "select title,content from article limit 10"


def load_data(data):
    data1 = jieba.cut(data)
    data11 = ""
    for item in data1:
        data11 += item + " "
    return data11


dataf = pda.read_sql(sql, conn)
dataf2 = dataf.T
title = dataf2.values[0]
content = dataf2.values[1]
train_text = []

for i in content:
    this_data = load_data(i)
    pat1 = "<[^<]*?>"
    this_data = re.sub(pattern=pat1, repl="", string=this_data)
    this_data = this_data.replace("\n", "").replace("\t", "")
    train_text.append(this_data)


# sklearn中的特征抽取类CountVectorizer
count_vect = CountVectorizer()
train_text_counts = count_vect.fit_transform(train_text)

from sklearn.feature_extraction.text import TfidfTransformer

tf_ts = TfidfTransformer(use_idf=True).fit(train_text_counts)
train_x_tf = tf_ts.transform(train_text_counts)

from sklearn.cluster import KMeans

kms = KMeans(n_clusters=3)
y = kms.fit_predict(train_x_tf)
dataf['type'] = y

dataf.to_csv("./01聚类归类法结果.csv",encoding="gbk")







