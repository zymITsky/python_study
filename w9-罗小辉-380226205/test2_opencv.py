import cv2
import numpy as npy

videoCapture = cv2.VideoCapture('data/finger.mp4')

if videoCapture.isOpened():
    v, frame = videoCapture.read()
else:
    rval = False

split = 1
n = 0
while v:
    print(n)
    v, frame = videoCapture.read()
    # 控制切分间隔
    if (n % split == 0):
        cv2.imwrite('data/finger/' + str(n) + '.jpg', frame)
    n += 1

videoCapture.release()
