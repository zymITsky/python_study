import cv2
import numpy as np

video = cv2.VideoCapture("data/finger.mp4")
width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
# CV_FOURCC('P', 'I', 'M', '1') = MPEG-1 codec
# CV_FOURCC('M', 'J', 'P', 'G') = motion-jpeg codec
# CV_FOURCC('M', 'P', '4', '2') = MPEG-4.2 codec
# CV_FOURCC('D', 'I', 'V', '3') = MPEG-4.3 codec
# CV_FOURCC('D', 'I', 'V', 'X') = MPEG-4 codec
# CV_FOURCC('U', '2', '6', '3') = H263 codec
# CV_FOURCC('I', '2', '6', '3') = H263I codec
# CV_FOURCC('F', 'L', 'V', '1') = FLV1 codec
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
fps = video.get(cv2.CAP_PROP_FPS)
'''
第一个参数表示需要生成的视频文件名
第二个参数表示fourcc
第三个参数表示fps 帧率
第四个参数表示大小
'''
videoWriter = cv2.VideoWriter('finger.avi', fourcc, 160, (width, height))
# videoWriter = cv2.VideoWriter('finger.avi', fourcc, fps, (width, height))
n = 0
while True:
    try:
        print(n)
        if (n == 1):
            pass
        img, frame = video.read()
        img_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # 用高斯滤波进行模糊处理，
        # 进行处理的原因：每个输入的视频都会因自然震动，光照变化或者摄像头本身等原因
        img_gauss = cv2.GaussianBlur(img_gray, (35, 35), 0)
        # 将第一帧设置为整个输入的背景
        if (n == 0):
            bg = img_gauss
        n += 1
        # 图像差别分析
        # 对于每个从背景之后读取的帧都会计算其与背景之间的差异，并得到一个差分图(diff)
        diff = cv2.absdiff(bg, img_gauss)
        # 还需要应用阀值来得到一副黑白图像
        diff = cv2.threshold(diff, 30, 255, cv2.THRESH_BINARY)[1]
        # 并通过dilate来膨胀(dilate)图像，从而对孔和缺陷进行归一化处理，getStructuringElement目的是取不同形态
        # 常用形状矩形(MORPY_RECT), 椭圆(MORPH_ELLIPSE), 十字形(MORPH_CROSS)
        es = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 3))
        diff = cv2.dilate(diff, es, iterations=2)
        # findContours检测物体轮廓，cv2.RETR_EXTERNAL代表只检测外轮廓，cv2.CHAIN_APPROX_SIMPLE表示压缩水平，垂直，对角线方向的元素
        # 只留下终点坐标，如矩形轮廓只需4个点来保存轮廓信息
        a, b, c = cv2.findContours(diff, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for i in b:
            # 计算面积
            if (cv2.contourArea(i) < 100):
                continue
            # 将i转为矩形的坐标
            x, y, width, height = cv2.boundingRect(i)
            if (width > height):
                r = width // 2
            else:
                r = height // 2
            # cv2.circle(frame, ((2 * x + width) // 2, (2 * y + height) // 2), r, (0, 0, 255), 2)
            cv2.rectangle(frame, (x, y), (x + width, y + height), (0, 0, 255), 2)
            font = cv2.FONT_HERSHEY_TRIPLEX
            cv2.putText(frame, r'hi,i am comming', (x, y), font, 1, (0, 0, 255), 1, False)
        cv2.imshow('diff', diff)
        cv2.imshow('frame', frame)
        # if(n%10 == 0):
        videoWriter.write(frame)
        cv2.waitKey(1)
    except Exception as err:
        print(err)
        break

video.release()
videoWriter.release()
cv2.destroyAllWindows()
