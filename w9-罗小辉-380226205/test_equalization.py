# 直方图均衡化

import cv2
import numpy as npy

pic1 = cv2.imread('data/copy.png')
pic2 = cv2.cvtColor(pic1, cv2.COLOR_BGR2GRAY)
pic3 = cv2.equalizeHist(pic2)
# 彩色图片如何进行直方图均衡化
'''
YUV:Y-->亮度信号
'''
pic4 = cv2.cvtColor(pic1, code=cv2.COLOR_BGR2YUV)
# 第三个参数0表示Y，1:U, 2:V
# pic4[:,:,0]表示一个三维数组，取该三维数组中第三维的数据，这里就是V了
pic4_y = pic4[:, :, 0]
pic4[:, :, 0] = cv2.equalizeHist(pic4_y)
pic5 = cv2.cvtColor(pic4, cv2.COLOR_YUV2BGR)

# cv2.imshow('pic2', pic2)
# cv2.imshow('pic3', pic5)

# 边检测器
# 1. 索贝尔(水平)
pic6 = cv2.Sobel(pic2, cv2.CV_64F, 1, 0, ksize=5)
# 2. 索贝尔(垂直)
pic7 = cv2.Sobel(pic2, cv2.CV_64F, 0, 1, ksize=5)
# 3.拉普拉斯
pic8 = cv2.Laplacian(pic2, cv2.CV_64F)
# 4.Canny边检测器
pic9 = cv2.Canny(pic2, 100, 255)
# 5.角检测器
pic10 = cv2.cornerHarris(pic2, 10, 5, 0.05)
# 角检测之后，把点显示出来
pic11 = cv2.imread('data/copy.png')
pic11[pic10 > 0.01 * pic10.max()] = [0, 0, 0]
# 特征点检测
# SIFT
sift = cv2.xfeatures2d.SIFT_create()
# point = sift.detect(pic2, None)
# cv2.drawKeypoints(pic1, point, pic1, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# Star-->模型迭代
# star = cv2.xfeatures2d.StarDetector_create()
# point = star.detect(pic2)
# cv2.drawKeypoints(pic1, point, pic1, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# star-sift迭代模型检测
star = cv2.xfeatures2d.StarDetector_create()
point = star.detect(pic2)
sift = cv2.xfeatures2d.SIFT_create()
point2, features = sift.compute(pic2, point)


# 特征提取
def getfetures(img):
    thisimg = cv2.imread(img)
    thisimg = cv2.resize(thisimg, (1024, 1024))
    thisimg_gray = cv2.cvtColor(thisimg, cv2.COLOR_BGR2GRAY)
    star = cv2.xfeatures2d.StarDetector_create()
    point1 = star.detect(thisimg_gray)
    sift = cv2.xfeatures2d.SIFT_create()
    point2, features = sift.compute(thisimg_gray, point1)
    return features

cv2.imshow('pic2', pic2)
cv2.imshow('pic9', pic1)

cv2.waitKey(0)
