import cv2
import numpy as npy

img = cv2.imread('data/test.jpg')
# cv2.imshow('julia', img)
# key = cv2.waitKey(0)
#
# if key == ord('t'):
#     print('Press t')

# 获取宽高
height, width = img.shape[:2]
# # 重新图片设置大小
# img2 = cv2.resize(img, (int(width / 2), int(height / 2)), interpolation=cv2.INTER_CUBIC)
# cv2.imshow('small julia', img2)

# 合并通道  先拆分成b,g,r，再合并
# b,g,r = cv2.split(img)
# img3 = cv2.merge((b,g,r))
# cv2.imshow('small julia', img3)

# 设置图片的颜色属性
# img4 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# img4 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# cv2.imshow('gray julia', img4)

#
M = npy.float32([[1, 0, 10], [0, 1, 20]])
img5 = cv2.warpAffine(img, M, (width, height))
cv2.imshow('gray julia', img5)

# 旋转, 缩放 180表示角度，1表示缩放比例
# M = cv2.getRotationMatrix2D((width / 2, height / 2), 180, 1)
# img6 = cv2.warpAffine(img, M, (width, height))
# cv2.imshow('rotation', img6)

# 拷贝
# cv2.imwrite('data/copy.png',img)

key = cv2.waitKey(0)
