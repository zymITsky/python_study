import cv2
import numpy as npy

cap = cv2.VideoCapture(0)
a, img = cap.read()
height, width = img.shape[:2]

x, y, height, width = int(width / 2), int(height / 2), 100, 100
rst_position = (x, y, width, height)
cv2.rectangle(img, (x, y), (x + width, y + height), (0, 0, 255))
# cv2.imshow("1", img)
# cv2.waitKey(0)

roi = img[y:y + height, x:x + width]
hsv_roi = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
# cv2.imshow("2", hsv_roi)
# cv2.waitKey(0)

nobg = cv2.inRange(hsv_roi, (0, 0, 70), (170, 255, 255))
# cv2.imshow("3",nobg)
hist = cv2.calcHist([hsv_roi], [0], None, [256], [0, 256])
cv2.normalize(hist, hist, 0, 255)
while True:
    a, img = cap.read()
    if a == True:
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # 计算反向投影
        destination_data = cv2.calcBackProject([hsv], [0], hist, [0, 256], 1)
        # MeanShift计算
        a, rst_position = cv2.meanShift(destination_data, rst_position, (3, 10, 1))
        x, y, width, height = rst_position
        cv2.rectangle(img, (x, y), (x + width, y + height), (0, 0, 255))
        # cv2.imshow("4", img)
        key = cv2.waitKey(1)
        if key == ord('q'):
            break
