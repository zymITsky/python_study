import cv2

pathmodel = 'data/haarcascade_frontalface_alt2.xml'
face = cv2.CascadeClassifier(pathmodel)

# 读取图片并进行灰度处理
img = cv2.imread('data/s.JPG')
img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# 检测人脸
result = face.detectMultiScale(img2,
                               scaleFactor=1.01,
                               minNeighbors=3,
                               minSize=(200, 200))
                               # flags=cv2.CASCADE_SCALE_IMAGE)

print(result)

# 标注人脸的位置
for x, y, width, height in result:
    print('x=' + str(x))
    print('y=' + str(y))
    cv2.rectangle(img, (x, y), (x + width, y + height), (0, 0, 255), 1)
cv2.imshow('w', img)
cv2.imwrite('data/face.jpg', img)

cv2.waitKey(0)
