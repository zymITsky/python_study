#coding=utf-8

# 图像识别文字(只能是png，而且只能识别数字)
import pytesseract
from PIL import Image

image = Image.open('images/code.png')
code = pytesseract.image_to_string(image)
print(code)