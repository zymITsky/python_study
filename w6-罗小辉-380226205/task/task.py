# coding=utf-8

# ---------------------------------------------和讯博客词云图绘制开始---------------------------------------------
# import wordcloud
# import matplotlib.pylab
# import pymysql
#
# conn = pymysql.connect(host="localhost",user="root",password="123456",db="hexun",charset="utf8")
# # 选择热门关键词
# sql = "select title from hx"
# cursor = conn.cursor()
# cursor.execute(sql)
#
# title = " "
# for index in cursor:
#     title += str(index[0]) + " "
#
# font = "simhei.ttf"
# mywc = wordcloud.WordCloud(collocations=False,font_path=font).generate(title)
# matplotlib.pylab.imshow(mywc)
# matplotlib.pylab.show()
# ---------------------------------------------和讯博客词云图绘制结束---------------------------------------------


# ---------------------------------------------自定义老九门词云图绘制开始---------------------------------------------
# import wordcloud
# import matplotlib.pylab
# import jieba
# from PIL import Image
# from numpy import array
# path = "老九门.txt"
# data = open(path,"r",encoding="gbk").read()
# cutdata=jieba.cut(data)
# allData = ""
# for index in cutdata:
#     allData += " " + str(index)
# font = "simhei.ttf"
# #读取图片
# cat = Image.open("cat.png")
# #图片转数组
# catArray = array(cat)
# mywc = wordcloud.WordCloud(collocations=False,font_path=font,mask=catArray,background_color="white").generate(allData)
# matplotlib.pylab.imshow(mywc)
# matplotlib.pylab.show()
# ---------------------------------------------自定义老九门词云图绘制结束---------------------------------------------


# ---------------------------------------------和讯博文数据分析散点图开始---------------------------------------------
# import pandas
# import numpy
# import matplotlib.pylab
# import wordcloud
# import jieba
# from PIL import Image
# import pymysql
#
# conn = pymysql.connect(host="localhost", user="root", password="123456", db="hexun", charset="utf8")
# readCountSql = "select readc from hx "
# commentCountSql = "select commentc from hx "
#
# readCount = pandas.read_sql(sql=readCountSql, con=conn)
# commentCount = pandas.read_sql(sql=commentCountSql, con=conn)
#
# # 横坐标-点击量，纵坐标-评论数
# font = matplotlib.font_manager.FontProperties(fname="simhei.ttf")
# xList = readCount.values.tolist()
# yList = commentCount.values.tolist()
#
# x = []
# y = []
# for index in range(len(xList)):
#     x.append(int(xList[index][0]))
#     y.append(int(yList[index][0]))
#
# x = sorted(x)
# y = sorted(y)
#
# # 绘制点击量-评论数散点图
# matplotlib.pylab.plot(x, y, "o")
# matplotlib.pylab.xlabel("点击量", fontproperties=font)
# matplotlib.pylab.ylabel("评论数", fontproperties=font)
# matplotlib.pylab.title("和讯博客原始数据[点击量-评论数]散点图", fontproperties=font)
# matplotlib.pylab.show()
# ---------------------------------------------和讯博文数据分析散点图结束---------------------------------------------


# ---------------------------------------------和讯博文数据异常分析开始---------------------------------------------
# 由于获取的和讯博客数据没有什么异常的，所以暂不处理
# ---------------------------------------------和讯博文数据异常分析结束---------------------------------------------




# ---------------------------------------------和讯博文评论数,点击量直方图开始---------------------------------------------
import pandas
import numpy
import matplotlib.pylab
import wordcloud
import jieba
from PIL import Image
import pymysql
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

conn = pymysql.connect(host="localhost", user="root", password="123456", db="hexun", charset="utf8")
readCountSql = "select readc from hx limit 1000"
commentCountSql = "select commentc from hx limit 1000"

readCount = pandas.read_sql(sql=readCountSql, con=conn)
commentCount = pandas.read_sql(sql=commentCountSql, con=conn)

# 横坐标-点击量，纵坐标-评论数
font = matplotlib.font_manager.FontProperties(fname="simhei.ttf")
xList = readCount.values.tolist()
yList = commentCount.values.tolist()

x = []  # 点击量
y = []  # 评论数
for index in range(len(xList)):
    x.append(int(xList[index][0]))
    y.append(int(yList[index][0]))

x = sorted(x)
y = sorted(y)

# 初始化数据ax0, ax1
fig, (ax0, ax1) = matplotlib.pylab.subplots(nrows=2, figsize=(9, 6))

# 第二个参数是柱子宽一些还是窄一些，越大越窄越密
ax0.hist(y, 40, normed=1, histtype='bar', facecolor='yellowgreen', alpha=0.75)
##讯博文评论数直方图
ax0.set_title('和讯博文评论数直方图', fontproperties=font)

ax1.hist(x, 20, normed=1, histtype='bar', facecolor='pink', alpha=0.75, cumulative=True, rwidth=0.8)
# 和讯博文点击量直方图
ax1.set_title('和讯博文点击量直方图', fontproperties=font)
fig.subplots_adjust(hspace=0.4)


matplotlib.pylab.show()

# ---------------------------------------------和讯博文评论数,点击量直方图结束---------------------------------------------
