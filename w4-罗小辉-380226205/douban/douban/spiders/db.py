# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request, FormRequest
import urllib.request
import requests
import os
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

class DbSpider(scrapy.Spider):
    name = 'db'
    allowed_domains = ['douban.com']
    print("111111111")
    # start_urls = ['http://douban.com/']
    header = {
        "User-Agent": "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Cookie': 'UM_distinctid=15fa02251e679e-05c01fdf7965e7-5848211c-144000-15fa02251e7800; bdshare_firstime=1510220189357; CNZZDATA1263415983=1653134122-1510216223-null%7C1510216223; CNZZDATA3866066=cnzz_eid%3D376479854-1494676185-%26ntime%3D1494676185; Hm_lvt_9a737a8572f89206db6e9c301695b55a=1510220189; Hm_lpvt_9a737a8572f89206db6e9c301695b55a=1510220990',
        'Pragma': 'no-cache',
    }

    def start_requests(self):
        print("22222222")
        return [Request("https://accounts.douban.com/login", meta={"cookiejar": 1}, headers=self.header,
                        callback=self.parse)]

    def parse(self, response):
        # 判断是否有验证码
        captcha = response.xpath("//img[@id='captcha_image']/@src").extract()
        if len(captcha) > 0:
            print("此时有验证码")
            # print(captcha[0])
            # 将验证码存储到本地
            localpath = "capture/"
            print(1)
            fileName = localpath + "capture.png"
            if os.path.exists(localpath) == False:
                print(2)
                os.makedirs(localpath)
                print(3)

            print(5)
            urllib.request.urlretrieve(captcha[0], fileName)
            print(4)
            captcha_value = input("到capture/capture.png下去查看。。。")

            data = {
                "captcha-solution": captcha_value,
                "redir": "https://www.douban.com/people/79217899/",
                "form_email": "15616177880",
                "form_password": "a15616073675",
            }
        else:
            print("没有验证码")
            # 设置要传递的post信息，此时没有验证码字段
            data = {
                "referer": "https://www.douban.com/people/79217899/",
                "redir": "https://www.douban.com/people/79217899/",
                "form_email": "15616177880",
                "form_password": "a15616073675",
            }

        print("登录中。。。。")
        # 通过FormRequest.from_response()进行登录
        return [FormRequest.from_response(response,
                                          # 设置cookie信息
                                          meta={"cookiejar": response.meta["cookiejar"]},
                                          # 设置headers信息模拟成浏览器
                                          headers=self.header,
                                          # 设置post表单中的数据
                                          formdata=data,
                                          # 设置回调函数，此时回调函数为next()
                                          callback=self.next,
                                          )]

    def next(self, response):
        title = response.xpath("/html/head/title/text()").extract()
        print("准备打印标题。。。")
        print(title)
        print("标题打印完全。。。")
