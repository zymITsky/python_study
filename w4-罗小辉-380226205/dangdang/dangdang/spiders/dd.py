# -*- coding: utf-8 -*-
import scrapy
# from dangdang.items import DangdangItem
import re
import requests

from dangdang.items import DangdangItem
from scrapy.http.request import Request


class DdSpider(scrapy.Spider):
    name = 'dd'
    allowed_domains = ['dangdang.com']
    # http: // category.dangdang.com / cid4010625.html
    start_urls = ['http://category.dangdang.com/cid10010336.html']

    def parse(self, response):
        title = response.xpath("//a[@name='itemlist-picture']/img/@alt").extract()
        price = response.xpath("//span[@class='price_n']/text()").extract()
        comment = response.xpath("//a[@name='itemlist-review']/text()").extract()
        link = response.xpath("//a[@name='itemlist-title']/@href").extract()
        item = DangdangItem()
        item['title'] = title
        item['price'] = price
        item['comment'] = comment
        item['link'] = link
        yield item
        for i in range(2, 101):
            url = "http://category.dangdang.com/pg" + str(i) + "-cid10010336.html"
            yield Request(url, callback=self.parse)
