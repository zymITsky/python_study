# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymysql


class DangdangPipeline(object):
    def process_item(self, item, spider):
        conn = pymysql.connect(host="localhost", user="root", password="123456", db="dangdang", charset="utf8")
        cursor = conn.cursor()
        print("=========================总共有" + str(len(item['title'])) + "条数据，准备开始写入到数据库中=========================")
        for i in range(0, len(item['title'])):
            title = item['title'][i]
            link = item['link'][i]
            comment = item['comment'][i]
            price = item['price'][i]
            sql = "insert into goods (title, price, link, comment) VALUES (\'%s\',\'%s\',\'%s\',\'%s\')" % (
                title, price, link, comment);
            try:
                print(sql)
                cursor.execute(sql)
                conn.commit()
            except:
                print(title + "写入失败。。。")
                conn.rollback()
        conn.close()
        return item
