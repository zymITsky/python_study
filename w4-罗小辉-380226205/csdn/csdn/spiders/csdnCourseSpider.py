# -*- coding: utf-8 -*-
import scrapy
import urllib.request
import re
from scrapy.http.request import Request

from csdn.items import CsdnItem


class CsdncoursespiderSpider(scrapy.Spider):
    name = 'csdnCourseSpider'
    allowed_domains = ['edu.csdn.net']
    start_urls = ['http://edu.csdn.net/courses/']

    def parse(self, response):
        # courseName = scrapy.Field()  # 课程名
        # studentCount = scrapy.Field()  # 学员人数
        # coursePrice = scrapy.Field()  # 课程价格
        # courseCount = scrapy.Field()  # 课程数量
        # courseLink = scrapy.Field()  # 课程链接地址
        # courseLecturer = scrapy.Field()  # 课程讲师

        courseName = response.xpath(
            "//dl[@class='course_lists clearfix']/dt/div[@class='titleInfor']/a/@title").extract()
        coursePrice = response.xpath("//dl[@class='course_lists clearfix']/dt/p[3]/i/text()").extract()
        courseCount = response.xpath("//dl[@class='course_lists clearfix']/dt/p[2]/em[1]/text()").extract()
        courseLink = response.xpath(
            "//dl[@class='course_lists clearfix']/dt/div[@class='titleInfor']/a/@href").extract()
        courseLecturer = response.xpath("//dl[@class='course_lists clearfix']/dt/p[1]/text()").extract()
        studentCount = self.getStudentCount(courseLink)

        item = CsdnItem()
        item['courseName'] = courseName
        item['studentCount'] = studentCount
        item['coursePrice'] = coursePrice
        item['courseCount'] = courseCount
        item['courseLink'] = courseLink
        item['courseLecturer'] = courseLecturer

        yield item
        for page in range(2, 232):
            url = "http://edu.csdn.net/courses/p" + str(page)
            yield Request(url, callback=self.parse)

    def getStudentCount(self, link):
        # 这里获取的link是一个数组，需要请求每一个链接地址，将学员人数读取出来，然后再将学员人数装到数组中
        counts = []
        for index in range(len(link)):
            data = self.get_html(link[index])
            # 使用正则
            reg = '<span class="num">(.*?)</span></span></div>'
            students = re.compile(reg, re.S).findall(data)
            counts.append(str(students[0]))
        return counts

    def get_html(self, url):
        return urllib.request.urlopen(url).read().decode("utf-8", "ignore")
