# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re
import pymysql


class CsdnPipeline(object):
    def process_item(self, item, spider):
        # courseName = scrapy.Field()  # 课程名
        # studentCount = scrapy.Field()  # 学员人数
        # coursePrice = scrapy.Field()  # 课程价格
        # courseCount = scrapy.Field()  # 课程数量
        # courseLink = scrapy.Field()  # 课程链接地址
        # courseLecturer = scrapy.Field()  # 课程讲师
        db = pymysql.connect("localhost", user="root", password="123456", db="csdn",charset="utf8")
        cursor = db.cursor()

        for index in range(len(item['courseLecturer'])):

            sql = "insert into courses (course_name,student_count,course_price,course_count,course_link,course_lecture) VALUES (\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')" \
                  % (item['courseName'][index], item['studentCount'][index],
                     item['coursePrice'][index].replace(" ", "").replace("\n", "")
                     , item['courseCount'][index], item['courseLink'][index], item['courseLecturer'][index]);

            try:
                cursor.execute(sql)
                db.commit()
                print(sql+"插入成功。。。。")
            except Exception as err:
                print("插入失败。。。。"+str(err))
                db.rollback()
        db.close()
        return item
